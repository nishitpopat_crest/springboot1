package cds.development.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {"cds.development.springboot.topic", "cds.development.springboot.course"})
@EntityScan(basePackages = {"cds.development.springboot.topic", "cds.development.springboot.course"})
@ComponentScan(basePackages = {"cds.development.springboot.topic", "cds.development.springboot.course"} )
@SpringBootApplication
public class CourseApiDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseApiDataApplication.class, args);
    }
}
